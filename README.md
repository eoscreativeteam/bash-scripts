# bash scripts

---

## Local.sh

**NOTE: Must have MAMP running and local user/database defined prior.**

Define `_NEW_PROJECT` variable.

This script will make directory in MAMP/htdocs folder, install wordpress, delete default themes, install divi & divi-child, delete unnecessary files, npm install / gulp, install plugins, delete unnecessary files.

You will need to set up user/database in `phpmyadmin` and run through WP isntall process. Problems with local install of WP-CLI currently prevents the process from being fully automated.

---

## Remote.sh

**NOTE: SSH into remote server (serverpilot@[ip.address]), then run script.**

Define `_PROJECT_NAME` variable.

This script will change directory to staging's themes, delete default themes, install divi & divi-child, delete unnecessary files, install plugins, delete unnecessary files, change directory to production's themes, repeat process

---
